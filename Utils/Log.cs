﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using static System.Console;

namespace Bergfall.Oculos.Utils
{
    public static class Log
    {
        public static void WriteLine(string message)
        {
            Console.WriteLine("(Thread {0}): {1}", Thread.CurrentThread.ManagedThreadId, message);
        }

        public static void WriteLine(string message, string filename)
        {
            File.AppendAllText(filename, message);
        }
        public static void WriteLine(int data)
        {
            WriteLine("Value = " + data);
        }



        public static void DisplayString(string text)
        {
            Console.WriteLine("String length: {0}", text.Length);
            foreach (char c in text)
            {
                if (c < 32)
                {
                    Console.WriteLine("<{0}> U+{1:x4}", (int)c);
                }
                else if (c > 127)
                {
                    Console.WriteLine("(Possibly non-printable) U+{0:x4}", (int)c);
                }
                else
                {
                    Console.WriteLine("{0} U+{1:x4}", c, (int)c);
                }
            }
        }

        public static void Debug(string message)
        {
            WriteLine(message);
        }

        public static void Debug(object message)
        {
            Debug(message.ToString());
        }

        public static void Write(object message)
        {
            write(message.ToString());
        }

        public static async void WriteToFile(IEnumerable<object> items)
        {
            using (StringWriter sw = new StringWriter())
            {
                foreach (var msg in items)
                {
                    await sw.WriteLineAsync(msg.ToString()).ConfigureAwait(false);
                }
                write(sw.ToString());
            }
        }

        private static async void write(string message)
        {
            string dir = Directory.GetCurrentDirectory() + @"\log.txt";

            using (StreamWriter sw = new StreamWriter(File.OpenWrite(dir)))
            {
                await sw.WriteAsync(message).ConfigureAwait(false);
            }
        }

        public static void Error(string message)
        {
            ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
        }
    }

}