namespace Bergfall.Oculos.Data
{
    public class TemplateToken
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int StartIndex { get; set; }
        public int Length { get; set; }
    }
}